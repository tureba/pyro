# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require setup-py [ import=distutils ] vim-doc

SUMMARY="Use vim as a frontend to GDB and PDB"
DESCRIPTION="
Pyclewn allows using vim as a front end to a debugger. Pyclewn currently
supports gdb and pdb.

The debugger output is redirected to a vim window, the pyclewn console. The
debugger commands are mapped to vim user-defined commands with a common letter
prefix, and with completion available on the commands and their first argument.
"
HOMEPAGE="http://pyclewn.sourceforge.net"
DOWNLOADS="
    python_abis:2.7? ( http://sourceforge.net/projects/${PN}/files/${PNV}/${PNV}.py2.tar.gz )
    python_abis:3.4? ( http://sourceforge.net/projects/${PN}/files/${PNV}/${PNV}.py3.tar.gz )
    python_abis:3.5? ( http://sourceforge.net/projects/${PN}/files/${PNV}/${PNV}.py3.tar.gz )
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        || (
            app-editors/gvim[python]
            app-editors/vim[python]
        )
    recommendation:
        sys-devel/gdb [[ description = [ GDB is required for C debugging ] ]]
"

BUGS_TO="pyromaniac@thwitt.de"

unpack_one_multibuild() {
    local abi=$(python_get_abi)
    unpack ${PNV}.py${abi:0:1}.tar.gz
    edo mv ${PNV}.py${abi:0:1} ${PNV}
}

prepare_one_multibuild() {
    # generating the helptags here violates sydbox.
    # Generating in postinst instead.
    edo sed -i -e '/pyclewn_install.build_vimhelp()/d' setup.py
    setup-py_prepare_one_multibuild
}

pkg_postinst() {
    update_vim_helptags
}

