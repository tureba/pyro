# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2
# Based on the Gentoo emelfm2-0.8.1.ebuild which is
#   Copyright 1999-2013 Gentoo Foundation

SUMMARY="Orthodox (Two-Paned) Filemanager"
DESCRIPTION="
This is a file manager for UNIX-like operating systems. It uses a simple and
efficient interface pioneered by Norton Commander, in the 1980s. The main window
is divided into three parts, described as 'panes' or 'panels'. Two of those
(side-by-side or top-to-bottom) show the contents of selected filesystem
directories. The third pane, at the bottom of the window, shows the output of
commands executed within the program. Those panes can be resized, and any one or
two of them can be hidden and unhidden, on request. A built-in command-line,
toolbar buttons or assigned keys can be used to initiate commands.
"

HOMEPAGE="http://emelfm2.net"
DOWNLOADS="${HOMEPAGE}/rel/${PNV}.tar.bz2"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        sys-apps/udisks:2
        sys-auth/polkit:*
        x11-libs/cairo
        x11-libs/gdk-pixbuf
        x11-libs/gtk+:3
        x11-libs/pango
"

BUGS_TO="pyromaniac@exherbo.org"

# The splint test fails with numerous errors
RESTRICT="test"

DEFAULT_SRC_PREPARE_PATCHES=(
    -p2 "${FILES}"/${PNV}-pkg-config.patch
    -p2 "${FILES}"/${PNV}-desktop.patch
)

DEFAULT_SRC_COMPILE_PARAMS=(
    BIN_DIR=/usr/$(exhost --target)/bin
    WITH_KERNELFAM=1
    WITH_GTK3=1
    WITH_POLKIT=1
    WITH_UDISKS=1
    STRIP=0
)

DEFAULT_SRC_INSTALL_PARAMS=(
    PREFIX=${IMAGE}/usr
    BIN_DIR=${IMAGE}/usr/$(exhost --target)/bin
    LIB_DIR=${IMAGE}/usr/$(exhost --target)/lib
)

