# Copyright 2015 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 ]
require gtk-icon-cache

SUMMARY="The owncloud client software"
HOMEPAGE="https://owncloud.com"
DOWNLOADS="https://download.owncloud.com/desktop/stable/${PNV}.tar.xz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    dolphin [[ description = [ Build dolphin extensions ] ]]
    shibboleth [[ description = [ Support Shibboleth authentication ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-db/sqlite:3[>=3.8.0]
        sys-auth/qtkeychain[qt5(+)]
        sys-libs/zlib
        x11-libs/qtbase:5[gui][sql][sqlite]
        dolphin? ( kde-frameworks/kio:5[>=5.16] )
        shibboleth? ( x11-libs/qtwebkit:5 )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl[>=1] )
    test:
        dev-util/cmocka
"

BUGS_TO="pyromaniac@exherbo.org"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/no-automagic-dolphin.patch
)
CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_WITH_QT4:BOOL=FALSE
    -DDATA_INSTALL_DIR:PATH=/usr/share
    -DSHARE_INSTALL_PREFIX:PATH=/usr/share
    -DSYSCONF_INSTALL_DIR:PATH=/etc
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'dolphin BUILD_SHELL_INTEGRATION_DOLPHIN'
    '!shibboleth NO_SHIBBOLETH'
)
CMAKE_SRC_CONFIGURE_TESTS=( '-DUNIT_TESTING:BOOL=TRUE -DUNIT_TESTING:BOOL=FALSE' )

src_install() {
    cmake_src_install
    nonfatal edo rmdir "${IMAGE}"usr/share/doc/mirall
    nonfatal edo rmdir "${IMAGE}"usr/share/man
}

src_test() {
    export HOME="${TEMP}"
    unset DISPLAY
    default
}

